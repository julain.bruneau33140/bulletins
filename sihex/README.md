## SI-Hex bulletin QuakeML formated from 1962-2009

Here is the dataset of seismological bulletin files in QuakeML format for the period 1962-2009 from the SI-Hex project (http://www.franceseisme.fr/sismicite.html).
The QuakeML conversion is a collaborative work between [CEA-LGD](http://www-dase.cea.fr) and [BCSF-Rénass](https://renass.unistra.fr), but still preliminary. Please report any problem opening a new *gitlab issue* !


Reference :

- **Cara, et. al, 2015**. *SI-Hex: a new catalogue of instrumental seismicity for metropolitan
France*. Bull. Soc. géol. France, 186 (1), pp. 3-19.