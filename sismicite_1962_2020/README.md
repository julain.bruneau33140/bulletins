## Note sur le catalogue de sismicité SI-Hex 1962-2009 complété par les années 2010-2020

Pour plus d'information consultez la [notice](note_carte_catalogue_sismicite_1962_2020.pdf).


### Carte de sismicité 1962-2020

En format PDF, jpg, png disponibles [ici](maps) 

<!-- ![jpg](maps/Sismicite_France_metropolitaine_1962_2020.jpg) -->
<img src="maps/Sismicite_France_metropolitaine_1962_2020.jpg" alt="map" width="300"/>

### Période 1962-2009

Le catalogue utilisé est celui du projet SI-Hex (http://www.franceseisme.fr/sismicite.html).

Référence :

- **Cara, et. al, 2015**. *SI-Hex: a new catalogue of instrumental seismicity for metropolitan
France*. Bull. Soc. géol. France, 186 (1), pp. 3-19.

### Sur la période 2010-2020 :

Le catalogue est complété par la sismicité enregistrée et localisée par le [BCSF-RéNaSS](https://renass.unistra.fr), dont les
magnitudes ont été converties en Mw selon la méthode décrite [ici](note_carte_catalogue_sismicite_1962_2020.pdf).



